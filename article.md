---
title: "Mapa: ČSSD ztrácela napříč republikou, ANO triumfuje"
perex: "Srovnání zisků jednotlivých stran v letošních krajských volbách ukazuje, že voliči odcházejí od sociálních demokratů k ANO 2011 Andreje Babiše."
description: "Srovnání zisků jednotlivých stran v letošních krajských volbách ukazuje, že voliči odcházejí od sociálních demokratů k ANO 2011 Andreje Babiše."
authors: ["Jan Cibulka"]
published: "9. října 2016"
# coverimg: https://interaktivni.rozhlas.cz/brexit/media/cover.jpg
# coverimg_note: "Foto <a href='#'>ČTK</a>"
url: "zisky-v-krajskych-volbach"
socialimg: https://interaktivni.rozhlas.cz/data/kraj-volby-zisky/www/media/socialimg.jpg
libraries: [jquery, "https://api.tiles.mapbox.com/mapbox-gl-js/v0.25.1/mapbox-gl.js", "https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v1.3.1/mapbox-gl-geocoder.js"]
styles: ["https://api.tiles.mapbox.com/mapbox-gl-js/v0.25.1/mapbox-gl.css", "https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v1.3.1/mapbox-gl-geocoder.css"]
recommended:
  - link: https://interaktivni.rozhlas.cz/preference-2016/
    title: Šampioni preferenčních hlasů: Kdo nejvíc získal, kdo nejvíc ztratil?
    perex: Sedmačtyřicet nově zvolených krajských zastupitelů získalo mandát jen díky „kroužkování“. Jiných sedmačtyřicet kandidátů ze stejného důvodu o místo v zastupitelstvu přišlo. Podívejte se, kteří to jsou a kterými kandidátkami preferenční hlasy zamíchaly nejvíce.
    image: https://interaktivni.rozhlas.cz/preference-2016/media/socialimg.jpg
  - link: https://interaktivni.rozhlas.cz/historie-brno/
    title: Volební mapa Brna: dominance lidovců dokáže přežít sto let a dvě diktatury
    perex: Politická paměť některých městských částí je nečekaně dlouhá. Dnešní lokální podporu křesťanských demokratů lze vystopovat až do prvních voleb za Rakouska-Uherska. Podobné vzorce platí i pro komunisty.
    image: https://interaktivni.rozhlas.cz/historie-brno/media/coverimg.jpg
  - link: https://interaktivni.rozhlas.cz/historie-krajskych-voleb/
    title: Doleva, či doprava. Podívejte se, jak volí vaše obec
    perex: Český rozhlas připravil aplikaci, kde zjistíte, čím je vaše obec v kraji výjimečná: Které strany zde vedou, a které naopak propadají.
    image: https://interaktivni.rozhlas.cz/historie-krajskych-voleb/media/socimg.jpg
  - link: https://interaktivni.rozhlas.cz/krajske-kandidatky/
    title: Chybí budoucí hejtmanky, nastupují protisystémové strany: prohlédněte si krajské kandidátky v grafech
    perex: Sociální demokraté postaví do voleb překvapivě mnoho žen, ale žádnou lídryni. ANO bude mít kandidátky téměř stoprocentně z politických nováčků a křesťanští demokraté jsou nejvzdělanější. Co o stranách říkají kandidátky?
    image: https://interaktivni.rozhlas.cz/krajske-kandidatky/media/socimg.png
---

Okolí Olomouce, Brna, Pelhřimova, Benešova nebo Jeseníku. Tam všude ČSSD ztrácela voliče ve srovnání s krajskými volbami v roce 2012. Ztráty, které partaj Bohuslava Sobotky utrpěla, se pohybují mezi 10 a 20 procentními body.

Srovnání volebních map pak ukazuje, že přesně na těch samých místech bodovalo ANO 2011, vedené ministrem financí Andrejem Babišem.

V následující mapě můžete prozkoumat volební zisky nebo ztráty všech stran, které kandidovaly v letošních krajských volbách a v těch před čtyřmi lety.

*Pokud se výsledky některých stran zobrazují pouze pro část území (jako např. u KDU-ČSL), může za to nejspíše fakt, že partaj v minulých volbách kandidovala v jiné koalici než letos. A jelikož ANO 2011 nastoupilo v krajských volbách poprvé (a není tedy s čím porovnávat), najdete [mapu jeho výsledků](#ano_map) samostatně dál v článku.*

_Modře jsou v mapě vyznačená místa, kde daná strana posílila, červeně obce, kde naopak ztratila._

<aside class="big">
<div id="mapa">
  <div id='selector'>
    <select class="selectVals"></select>
  </div>
  <div id='info'>Najetím na mapu vyberte obec.</div>
  <div id='map'></div>
</div>
</aside>

_Zdroj: [Volby.cz](http://volby.cz)_

Občanští demokraté oslabili hlavně na Plzeňsku, evidentně je pryč kouzlo oblíbeného Jiřího Pospíšila, která zde vedl kandidátku v roce 2012. Ten letos kandidoval na posledním místě plzeňské kandidátky TOP 09, [preferenční hlasy ho nicméně poslaly do kraje i tak](https://interaktivni.rozhlas.cz/preference-2016/). Prakticky všude jinde ale ODS mírně posilovala, částečně tak vyrovnala debakl po „oranžovém tsunami“ před čtyřmi roky.

Křesťanští demokraté uspěli pod vedením Jiřího Čunka na Zlínsku, ztráceli naopak v okolí Brna. Ve zbytku republiky pak kandidovali vždy v koalici (proto se zobrazuje jen část mapy).

Z menších stran pak téměř všude ztráceli Piráti, Svobodní zůstali na podobných číslech jako v minulosti. Extremistické strany jako Sládkovci či Dělnická strana sice nekandidovaly plošně, lokálně ale posílily.

Pokud se pak vrátíme zpět k duelu ANO a ČSSD, následující mapa výsledků Andreje Babiše prakticky shodně kopíruje opačný trend u sociální demokracie.

<aside class="big">
  <img id="ano_map" src="https://interaktivni.rozhlas.cz/data/kraj-volby-zisky/www/media/ano.jpg">
</aside>